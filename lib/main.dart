import 'package:flutter/material.dart';
import './negara-asean.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Satu',
      home: NegaraAsean(),
      theme: ThemeData.dark(),
    );
  }
}

class NegaraAsean extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Neagara Asean'),
      ),
      body: Center(
        child: SingleChildScrollView(
          padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              for (var item in negaraAsean)
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 16),
                  child: Row(
                    children: [
                      ClipRRect(
                        borderRadius: BorderRadius.circular(8.0),
                        child: Image.asset(item['bendera'],
                            height: 70, width: 110, fit: BoxFit.cover),
                      ),
                      Expanded(
                        child: Container(
                            margin: EdgeInsets.only(left: 16.0),
                            height: 70,
                            decoration: BoxDecoration(
                                color: Color(0xff3f3f3f),
                                shape: BoxShape.rectangle,
                                borderRadius: BorderRadius.circular(8)),
                            child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    item['nama'].toUpperCase(),
                                    style: TextStyle(
                                        fontSize: 17,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  Text(
                                    item['ibuKota'].toUpperCase(),
                                  )
                                ])),
                      )
                    ],
                  ),
                )
            ],
          ),
        ),
      ),
    );
  }
}
