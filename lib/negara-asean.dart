const negaraAsean = [
  {
    'nama': 'Brunei Darussalam',
    'ibuKota': 'Bandar Seri Begawan',
    'bendera': 'assets/images/flag_of_brunei.png'
  },
  {
    'nama': 'Kamboja',
    'ibuKota': 'Phnom Penh',
    'bendera': 'assets/images/flag_of_cambodia.png'
  },
  {
    'nama': 'Indonesia',
    'ibuKota': 'Jakarta',
    'bendera': 'assets/images/flag_of_indonesia.png'
  },
  {
    'nama': 'Laos',
    'ibuKota': 'Vientiane',
    'bendera': 'assets/images/flag_of_laos.png'
  },
  {
    'nama': 'Malaysia',
    'ibuKota': 'Kuala Lumpur',
    'bendera': 'assets/images/flag_of_malaysia.png'
  },
  {
    'nama': 'Myanmar',
    'ibuKota': 'Nay Pyi Taw',
    'bendera': 'assets/images/flag_of_myanmar.png'
  },
  {
    'nama': 'Singapura',
    'ibuKota': 'Singapura',
    'bendera': 'assets/images/flag_of_singapore.png'
  },
  {
    'nama': 'Thailand',
    'ibuKota': 'Bankok',
    'bendera': 'assets/images/flag_of_thailand.png'
  },
  {
    'nama': 'Filipina',
    'ibuKota': 'Manila',
    'bendera': 'assets/images/flag_of_the_philippines.png'
  },
  {
    'nama': 'Vietnam',
    'ibuKota': 'Hanoi',
    'bendera': 'assets/images/flag_of_vietnam.png'
  }
];
