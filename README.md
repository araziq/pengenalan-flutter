# Pengenalan Flutter

Tugas pengenalan Flutter - membuat Aplikasi yang menampilkan negara-neagara ASEAN.

## Hasil

Aplikasi ini dibuat dengan dua tampilan berbeda, yaitu tampilan standar pada [branch master](https://gitlab.com/araziq/pengenalan-flutter/-/tree/master) dan tampilan card pada [branch versi card](https://gitlab.com/araziq/pengenalan-flutter/-/tree/versi-card).

### Versi standar

Preview

![Demo](docs/video/demo1.mp4)

### Versi Card

Preview

![Demo](docs/video/demo2.mp4)
